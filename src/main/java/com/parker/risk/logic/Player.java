package com.parker.risk.logic;

import com.parker.risk.logic.cards.Card;
import com.parker.risk.logic.pieces.Infantry;
import com.parker.risk.logic.pieces.Piece;
import com.parker.risk.logic.util.Util;

import java.util.ArrayList;

public class Player {

    private String name;
    private ArrayList<Card> cards;
    private ArmyColor color;
    private ArrayList<Piece> pieces;
    private int order = 0;

    public Player(String name, ArmyColor color) {
        this.name = name;
        this.color = color;
        this.pieces = new ArrayList<>();
        this.order = Util.getRandom(1, 1000);
    }

    public String getName() {
        return name;
    }

    public ArmyColor getColor() {
        return color;
    }

    public int getOrder() {
        return order;
    }

    public void addCard(Card card) {
        this.cards.add(card);
    }

    public void addInfantry(int quantity) {
        for (int i = 0; i < quantity; i++) {
            pieces.add(new Infantry(this.color));
        }
    }

    public Infantry getInfantry() {
        Piece piece = null;
        for (Piece item: pieces) {
            if (item instanceof Infantry) {
                piece = item;
                break;
            }
        }
        if (pieces.remove(piece)) {
            return (Infantry) piece;
        }
        return null;
    }
}
