package com.parker.risk.logic;

import com.parker.risk.logic.util.Util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumSet;

// https://www.hasbro.com/common/instruct/risk.pdf
public class Game {
    private Board board;
    private ArrayList<Player> players;
    private boolean started = false;
    private ArrayList<ArmyColor> colors;

    public Game() {
        this.board = new Board();
        this.players = new ArrayList<>();
        this.colors = new ArrayList<>(EnumSet.allOf(ArmyColor.class));
    }

    public void start() {
        int infantry = 40;
        infantry -= (5 * (players.size() - 2));

        for (Player player: players) {
            player.addInfantry(infantry);
        }

        Collections.sort(players, new Comparator<Player>() {
            @Override
            public int compare(Player player0, Player player1) {
                if (player0.getOrder() > player1.getOrder()) {
                    return 1;
                }
                else if (player0.getOrder() < player1.getOrder()) {
                    return -1;
                }
                return 0;
            }
        });
    }

    public void addPlayer(String name) {
        if (!started && players.size() < 6) {
            Player player = new Player(name, colors.remove(Util.getRandom(0, colors.size() - 1)));
            players.add(player);
        }
    }

    public void randomTerritoryClaim() {
        while (board.getUnclaimedTerritories().size() > 0) {
            for (Player player: players) {
                ArrayList<Territory> unclaimedTerritories = board.getUnclaimedTerritories();
                Territory territory = unclaimedTerritories.get(Util.getRandom(0, unclaimedTerritories.size() - 1));
                board.claimTerritory(territory, player);
            }
        }
    }
}
