package com.parker.risk.logic;

import com.parker.risk.logic.util.Util;

public class Dice {

    public int sides;

    public Dice(int sides) {
        this.sides = sides;
    }

    public int roll() {
        return Util.getRandom(1, this.sides);
    }

}
