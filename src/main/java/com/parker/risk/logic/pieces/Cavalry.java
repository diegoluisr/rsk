package com.parker.risk.logic.pieces;

import com.parker.risk.logic.ArmyColor;

public class Cavalry extends Piece{

    public Cavalry() {

    }

    public Cavalry(ArmyColor color) {
        super(color);
    }
}
