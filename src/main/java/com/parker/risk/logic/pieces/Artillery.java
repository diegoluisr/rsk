package com.parker.risk.logic.pieces;

import com.parker.risk.logic.ArmyColor;

public class Artillery extends Piece{

    public Artillery(){

    }

    public Artillery(ArmyColor color) {
        super(color);
    }
}
