package com.parker.risk.logic.pieces;

import com.parker.risk.logic.ArmyColor;

public class Piece {

    private ArmyColor color;

    public Piece() {

    }

    public Piece(ArmyColor color) {
        this.color = color;
    }
}
