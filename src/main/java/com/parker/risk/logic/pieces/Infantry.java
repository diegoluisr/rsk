package com.parker.risk.logic.pieces;

import com.parker.risk.logic.ArmyColor;

public class Infantry extends Piece{

    public Infantry() {

    }
    public Infantry(ArmyColor color) {
        super(color);
    }
}
