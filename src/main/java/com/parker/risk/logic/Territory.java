package com.parker.risk.logic;

import com.parker.risk.logic.pieces.Piece;

import java.util.ArrayList;

public class Territory {

    private int id;
    private String name;
    private TerritoryGroup group;
    private ArrayList<Piece> army;
    private ArrayList<Territory> borders;

    public Territory(int id, String name, TerritoryGroup group) {
        this.id = id;
        this.name = name;
        this.group = group;
        this.army = new ArrayList<>();
        this.borders = new ArrayList<>();
    }

    public void addBorder(Territory border) {
        this.borders.add(border);
        if (!border.getBorders().contains(this)) {
            border.addBorder(this);
        }
    }

    public ArrayList<Territory> getBorders() {
        return borders;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public TerritoryGroup getGroup() {
        return group;
    }

    public void setGroup(TerritoryGroup group) {
        this.group = group;
    }

    public void addArmy(Piece piece) {
        this.army.add(piece);
    }

    public boolean isOccupied() {
        if (this.army.size() > 0) {
            return true;
        }
        return false;
    }
}
